package controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.user.User;
import view.ViewFactory;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoginController{
    //elements from Pane - SighnInPane
    public Pane SighnInPane;
    public Button RegistrationInButton;
    public Label HelloLabel;
    public TextField LogInTextfield;
    public TextField PasInTextField;
    public Label AccountInLabel;
    public Button EntranceInButton;

    //elements from Pane - SighnUpPane
    public Pane SignUpPane;
    public Button EntranceUpButton;
    public Label RegistrationUpLabel;
    public Label CheckPasUpLabel;
    public TextField LoginUpTextField;
    public TextField Pas2UpTextField;
    public TextField CheckUpTextField;
    private User user = new User();

    Map<String,String> usersLoginAndPass = new LinkedHashMap<>();


//    public void getLogin() {
//        userUp.setLogin(LoginUpTextField.getText());
//
//    }
//
//    public void getPassword(){
//        userUp.setLogin(Pas2UpTextField.getText());
//    }



    //Забираем с полей значения и проверяем если все хорошо переправляем в другое окошко
    public void signin() throws Exception {
        String login = LogInTextfield.getText();
        String password = PasInTextField.getText();
        if (login.equals("admin") && password.equals("1234")){
            changeView("admin");
        }else if (usersLoginAndPass.containsKey(login) && usersLoginAndPass.containsValue(password)){
            changeView("UserInfoView");
        }
    }

    //Если нажали кнопку зарегистрироваться тогдапереходим на регистрацию
    public void goToSignup() {
        SighnInPane.setVisible(false);
        SignUpPane.setVisible(true);
    }


    //в регистрации проверяем чтобы поля были не пустыми если все хорошо регистрируем в нашей колекции
    public void signup1() throws Exception {
        String login = LogInTextfield.getText();
        String password = PasInTextField.getText();
        if (!login.equals("") && !password.equals("")){
            usersLoginAndPass.put(login,password);
            SignUpPane.setVisible(false);
            SighnInPane.setVisible(true);
        }

    }

    //переход на новую страницу например Chat - пока что Вам это не нужно.
    public void changeView(String view) throws Exception {
        ViewFactory.getController(view).start(new Stage(StageStyle.DECORATED));
        Stage stage = (Stage) SighnInPane.getScene().getWindow();
        stage.close();
    }



//переход на страницу юзера
    public void signup(ActionEvent actionEvent) throws IOException,ClassNotFoundException{
        String login = LoginUpTextField.getText();
        String password = Pas2UpTextField.getText();
        String password2= CheckUpTextField.getText();
        if (login.equals("")|| password.equals("")||(!password.equals(password2))){
            System.out.println(" Your login or your password  is empty or your passwords aren't the same");
            System.out.println(" Enter new password or new login");
        }
        else{
       User user=new User(login,password);
        try {
            changeView("UserInfo");
        } catch (Exception e) {
            e.printStackTrace();}
        }

//        userDB.saveTo(user);
// listUserReg.add(userUp);

//        UserDB userDB=new UserDB();
//        userUp.setLogin(LoginUpTextField.getText());
//        userUp.setPassword(Pas2UpTextField.getText());

//        userDB.readTo();

//        System.out.println("Login  "+ userUp.getLogin());
//        System.out.println("Password  "+ userUp.getPassword());


    }

//    public void
}
