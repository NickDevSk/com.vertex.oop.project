package controllers;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.admin.Admin;
import model.client.core.ClientStart;
import model.client.core.ClientWorker;
import model.connection.Connection;
import model.connection.DataPackage;
import model.connection.DataPackageFactory;
import view.ViewFactory;

import java.net.Socket;
import java.util.Scanner;


public class AdminController{
    public TextField pathHistoryField;
    public TextField serverIPField;
    public TextField serverPortField;
    public Button adminStart;
    public Button adminCancel;
    public Pane pane;
    private Admin admin = new Admin();


    public void adminStart() throws Exception {

//        admin.setIp(serverIPField.getText());


//        admin.setPort(serverPortField.getText());
      //  ad.setFolder(pathHistoryField.getText());
        if(!admin.setPort(serverPortField.getText())){
            Text text =  new Text();
            text.setText(serverPortField.getText());
            text.setFill(Color.RED);
            serverPortField.setText(text.getText());
        }else if(true){

        }else{

        }

        ClientWorker clientWorker = new ClientWorker(admin.getConnection());
        clientWorker.start();

        clientWorker.sendDataPackage(new DataPackageFactory(admin.getConnection()).getHelloDataPackage());

        Stage stage = (Stage) pane.getScene().getWindow();
        stage.close();
        ViewFactory.getController("login").start(new Stage(StageStyle.DECORATED));
    
    }

    public void adminCancel(ActionEvent actionEvent) {
    }




}
