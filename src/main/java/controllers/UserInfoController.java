package controllers;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import model.serverServer.users.UserDB;
import model.user.User;
import view.UserInfoView;

import java.io.IOException;
import java.util.ArrayList;

public class UserInfoController{
    public TextField userNickname;
    public ChoiceBox userSex;
    public TextField userPathArea;
    public TextField userOldPassword;
    public TextField userNewPassword;
    public TextArea userInfo;
    public ImageView userPhoto;
    public Pane userChangePasswordPane;
    public Pane userChangeImagePane;
    public Button userSaveButton;


    private UserInfoView userInfoView = new UserInfoView();

    public void saveData() {
        userInfoView.setUserNickname(userNickname.getText());
    }

    public void createInfo() {
        userInfoView.setUserInfo(userInfo.getText());
    }

    public void setSex(){
        userSex.setItems(FXCollections.observableArrayList(
                "Female","Male"
        ));
    }

    public void createNickname() {
        userInfoView.setUserNickname(userNickname.getText());

    }

    public void userChangePhoto() {
        userChangeImagePane.setVisible(true);
        userChangeImagePane.setDisable(false);
    }

    public void userSavePath() {
        userChangeImagePane.setVisible(false);
        userChangeImagePane.setDisable(true);
    }

    public void userChangePassword() {
        userChangePasswordPane.setVisible(true);
        userChangePasswordPane.setDisable(false);
    }

    public void userEnterOldPassword() {
        //promptText 1. user.ChangePassword(ответ от Тани) 2.
    }

    public void userEnterNewPassword() {

    }

    public void userSavePassword() {
        userChangePasswordPane.setVisible(false);
        userChangePasswordPane.setDisable(true);
    }

// кнопка сохраняет информацию о юзере
    public void setUserSaveButton(ActionEvent actionEvent) throws IOException,ClassNotFoundException{
        UserDB userDB=new UserDB();
        ArrayList<User> listUserReg = new ArrayList<User>();
        String nickName=userNickname.getText();
        String info= userInfo.getText();
        Image photoPath=userPhoto.getImage();
        boolean sex= userSex.isShowing();
        User user=new User(nickName,photoPath,info,sex);
        userDB.saveTo(user);

    }


}
