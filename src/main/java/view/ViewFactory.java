package view;

public class ViewFactory {
    public static View getController(String view){
        if (view.equals("")){
            return null;
        }else if(view.equalsIgnoreCase("admin")){
            return new AdminView();
        }else if(view.equalsIgnoreCase("chat")){
            return new ChatView();
        }else if(view.equalsIgnoreCase("login")){
            return new LoginView();
        }else if(view.equalsIgnoreCase("userinfo")){
            return new UserInfoView();
        }else {
            return null;
        }
    }
}
