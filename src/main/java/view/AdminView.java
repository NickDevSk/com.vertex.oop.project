package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AdminView extends View {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/admin.fxml"));
        primaryStage.setTitle("Admin Panel");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
