package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ChatView extends View{
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/chat.fxml"));
        primaryStage.setTitle("Chat Panel");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
