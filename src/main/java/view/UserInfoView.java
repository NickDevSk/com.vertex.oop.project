package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class UserInfoView extends View{

    String userNickname;
    String userInfo;

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public void setUserInfo(String userInfo){
        this.userInfo = userInfo;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/userinfo.fxml"));
        primaryStage.setTitle("User Info Panel");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
