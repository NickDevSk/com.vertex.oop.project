package model.admin;

import model.client.core.ClientStart;
import model.connection.Connection;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

public class Admin implements AdminDao{

    private String ip = null; //"0.112.5.117";//"0.145.123.117";
    private int port;
    private String folder;
    ParserAbstract p = null;
    Connection connection = null;


    public Connection getConnection() throws IOException {
        if (connection==null) {
            Socket socket = new Socket(this.getIp(), this.getPort());
            this.connection = new Connection(socket);
        }
        return connection;
    }


    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        File file = new File(folder);
        this.folder = folder;
    }

    public boolean setPort(String port){
        p = new PortParser();
        if((p.validate(port))){
            this.port = p.getValidatePort();
            return true;
        }else {
            return false;
        }


    }
    public int getPort(){
        return port;
    }

    public String getIp() {
            return ip;
    }

    public void setIp(String ip) {
        try {
            p = new IPParser();
            if (p.validate(ip))
                this.ip = ip;
        } catch (NumberFormatException e) {
            e.getMessage();
        }
    }
}
