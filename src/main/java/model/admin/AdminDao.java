package model.admin;

public interface AdminDao {
    public abstract String getIp();

    public abstract void setIp(String ip);

    public abstract int getPort();

    public abstract boolean setPort(String port);

    public abstract String getFolder();

    public abstract void setFolder(String folder);
}
