package model.admin;

public abstract class ParserAbstract {
    public abstract boolean validate(final String value);
    public abstract Integer getValidatePort();
}
