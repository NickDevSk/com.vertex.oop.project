package model.admin;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class IPParser extends ParserAbstract {
    private Pattern pattern;
    private Matcher matcher;

    private static final String IPADDRESS_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public IPParser() {
        pattern = Pattern.compile(IPADDRESS_PATTERN);
    }

    /**
     * Validate ip address with regular expression
     *
     * @param ip ip address for validation
     * @return true valid ip address, false invalid ip address
     */
    public boolean validate(final String ip) {
        matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    @Override
    public Integer getValidatePort() {
        return null;
    }

}



           /* try{
                if (ip == null || ip.isEmpty())
                    return false;
                // return false;
                for (int i = 0; i < ip.length(); i++) {
                    if (ip.charAt(i) == '/' || ip.charAt(i) == '\\' || ip.charAt(i) == '$')
                        return false;
                    //return false;
                }

                String[] path = ip.split("\\.", -1);
                if (path.length != 4) {
                    return false;
                    //return false;
                }
                for (String s : path) {

                    int i = Integer.parseInt(s);
                    if (i < 0 || i > 255) {
                        return false;
                        //return false;
                    }
                }

                this.ip = ip;
            }catch (NumberFormatException e){
                e.getMessage();
            }


        return true;
    }*/

