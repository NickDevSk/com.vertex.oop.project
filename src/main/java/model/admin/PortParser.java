package model.admin;

public class PortParser extends ParserAbstract {
    int port;
    @Override
    public boolean validate(String port) {
        int portInt = Integer.parseInt(port);
        if(portInt >= 0 && portInt < 65536) {
            this.port = portInt;
            return true;
        }else {
            System.out.println("return false");
            return false;
        }
    }

    @Override
    public Integer getValidatePort(){
        return port;
    }
}
