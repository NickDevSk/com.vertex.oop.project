package model.connection;

import model.commands.Command;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Connection {
    Socket socket = null;
    private ConcurrentLinkedQueue<DataPackage> sendDataPackageQueue = null;
    public ConcurrentLinkedQueue<DataPackage> receivedDataPackageQueue = null;
    SendConnectorThread sendConnectorThread = null;
    ReceiveConnectorThread receiveConnectorThread = null;
    public final int connectionId;
    private static int nextConnectionId = 0;
    private volatile boolean objectOutputStreamClosed = false;
    private volatile boolean objectInputStreamClosed = false;

    {
        connectionId = nextConnectionId;
        nextConnectionId += 1;
    }

    public Connection(Socket socket) {
        if ( socket != null ) {
            this.socket = socket;
            this.sendDataPackageQueue = new ConcurrentLinkedQueue<>();;
            this.receivedDataPackageQueue = new ConcurrentLinkedQueue<>();;
            sendConnectorThread = new SendConnectorThread(socket, sendDataPackageQueue, this);
            sendConnectorThread.start();
            receiveConnectorThread = new ReceiveConnectorThread(socket, receivedDataPackageQueue, this);
            receiveConnectorThread.start();
        } else {
            System.err.println("Не могу создать Connection, т.к. socket == null.");
        }
    }

    public boolean closeConnection() {
        if ( receiveConnectorThread.isConnectionAlive() ) {
            sendDataPackage(new DataPackage(Command.DISCONNECT));
        }

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sendConnectorThread.setConnectionAlive(false);

        while ( ( ! receiveConnectorThread.isThreadCanBeStop() ) & ( ! sendConnectorThread.isThreadCanBeStop() ) ) {
            receiveConnectorThread.setThreadCanBeStop(true);
            sendConnectorThread.setThreadCanBeStop(true);
        }

        while ( ( ! objectOutputStreamClosed ) & ( ! objectInputStreamClosed ) ) { }

        closeSocket();

        return true;
    }

    private void closeSocket() {
        if ( socket != null ) {
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println("Ошибка во время попытки закрыть сокет.");
                e.printStackTrace();
            }
        }
    }

    public String getConnectionLocalAddress(){
        return socket.getLocalAddress().toString();
    }

    public String getConnectionRemoteSocketAddress(){
        return socket.getRemoteSocketAddress().toString();
    }

    public void sendDataPackage(DataPackage dataPackage) {
        if ( dataPackage != null ) {
            while ( ! sendDataPackageQueue.offer(dataPackage) ) {

            }
        }
    }

    public boolean setObjectOutputStreamClosed(boolean objectOutputStreamClosed) {
        this.objectOutputStreamClosed = objectOutputStreamClosed;

        return true;
    }

    public boolean setObjectInputStreamClosed(boolean objectInputStreamClosed) {
        this.objectInputStreamClosed = objectInputStreamClosed;

        return true;
    }
}
