package model.connection;

import model.commands.Command;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ReceiveConnectorThread extends Thread {
    Socket socket = null;
    public ObjectInputStream ois = null;
    private volatile boolean connectionAlive = false;
    private volatile boolean threadCanBeStop = false;
    DataPackage dataPackage = null;
    ConcurrentLinkedQueue<DataPackage> receivedDataPackageQueue = null;
    Connection connection = null;

    public ReceiveConnectorThread(Socket socket, ConcurrentLinkedQueue<DataPackage> receivedDataPackageQueue, Connection connection) {
        this.socket = socket;
        this.receivedDataPackageQueue = receivedDataPackageQueue;
        connectionAlive = true;
        this.connection = connection;

        String tmpThreadName = this.getName();
        this.setName("ReceiveConnectorThread_" + tmpThreadName);
    }

    @Override
    public void run() {
        if ( socket != null ) {
            try {
                ois = new ObjectInputStream(socket.getInputStream());

                while ( connectionAlive ) {
                    try {
                        dataPackage = (DataPackage) ois.readObject();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    while ( ! receivedDataPackageQueue.offer(dataPackage) ) {
                    }

                    if ( dataPackage.getCommandName().equals(Command.DISCONNECT.toString()) ) {
                        connectionAlive = false;
                        break;
                    }
                }

                while ( ! threadCanBeStop ) {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                System.err.println("Ошибка во время работы с коннектором получения.");
                e.printStackTrace();
            } finally {
                if ( ois != null ) {
                    try {
                        ois.close();
                        while ( ! connection.setObjectInputStreamClosed(true) ) {

                        }
                    } catch (IOException e) {
                        System.err.println("Ошибка во время попытки закрытия коннектора получения.");
                        e.printStackTrace();
                    }
                }
            }
        } else {
            System.err.println(this.getName() + " : Не могу запустить поток коннектора получения. Сокет == null.");
        }
    }

    public boolean isConnectionAlive() {
        return connectionAlive;
    }

    public void setConnectionAlive(boolean connectionAlive) {
        this.connectionAlive = connectionAlive;
    }

    public boolean isThreadCanBeStop() {
        return threadCanBeStop;
    }

    public void setThreadCanBeStop(boolean threadCanBeStop) {
        this.threadCanBeStop = threadCanBeStop;
    }
}
