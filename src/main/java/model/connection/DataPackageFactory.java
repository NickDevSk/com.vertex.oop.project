package model.connection;


import model.commands.Command;
import model.commands.dataOfDataPackage.Disconnect;
import model.commands.dataOfDataPackage.authorize.AuthorizeResponseState;
import model.commands.dataOfDataPackage.change.userName.ChangeUserName;
import model.commands.dataOfDataPackage.change.userName.ChangeUserNameResponse;
import model.commands.dataOfDataPackage.change.userName.ChangeUserNameResponseState;
import model.commands.dataOfDataPackage.change.userPassword.ChangeUserPassword;
import model.commands.dataOfDataPackage.change.userPassword.ChangeUserPasswordResponseState;
import model.commands.dataOfDataPackage.change.userPhoto.ChangeUserPhoto;
import model.commands.dataOfDataPackage.change.userPhoto.ChangeUserPhotoResponseState;
import model.commands.dataOfDataPackage.hello.Hello;
import model.commands.dataOfDataPackage.hello.HelloResponse;
import model.commands.dataOfDataPackage.register.RegisterResponse;
import model.commands.dataOfDataPackage.register.RegisterResponseState;
import model.commands.dataOfDataPackage.textMessage.TextMessage;
import model.user.User;

public class DataPackageFactory {
    Connection connection = null;

    public DataPackageFactory(Connection con) {
        if ( con != null ){
            connection = con;
        } else {
            System.err.println("Не могу создать DataPackageFactory. Connection == null.");
        }
    }

    public DataPackage getAuthorizeDataPackage(User user) {
        if ( user != null ) {
            return (new DataPackage(Command.AUTHORIZE, user));
        } else {
            System.err.println("DataPackageFactory.getAuthorizeDataPackage(): Не могу создать DataPackage. user == null.");
        }

        return null;
    }

    public DataPackage getAuthorizeResponseDataPackage(AuthorizeResponseState state) {
        if ( state != null ) {
            return (new DataPackage(Command.AUTHORIZERESPONSE, state));
        } else {
            System.err.println("DataPackageFactory.getAuthorizeResponseDataPackage(): Не могу создать DataPackage. AuthorizeResponseState == null.");
        }

        return null;
    }

    public DataPackage getRegisterDataPackage(User user) {
        if ( user != null ) {
            return (new DataPackage(Command.REGISTER, user));
        } else {
            System.err.println("DataPackageFactory.getRegisterDataPackage(): Не могу создать DataPackage. user == null.");
        }

        return null;
    }

    public DataPackage getRegisterResponseDataPackage(RegisterResponseState state, int userIdRecievedFromServer) {
        if ( state != null ) {
            return (new DataPackage(Command.REGISTERRESPONSE, new RegisterResponse(state, userIdRecievedFromServer)));
        } else {
            System.err.println("DataPackageFactory.getRegisterResponseDataPackage(): Не могу создать DataPackage. AuthorizeResponseState == null.");
        }

        return null;
    }

    public DataPackage getHelloDataPackage() {
        if ( connection != null ) {
            return new DataPackage(Command.HELLO, new Hello(connection.getConnectionLocalAddress()));
        } else {
            System.err.println("DataPackageFactory.getHelloDataPackage(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getHelloResponseDataPackage() {
        if ( connection != null ) {
            return new DataPackage(Command.HELLORESPONSE, new HelloResponse(connection.getConnectionLocalAddress()));
        } else {
            System.err.println("DataPackageFactory.getHelloResponseDataPackage(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getDisconnectDataPackage(User userSelfObject) {
        if ( connection != null ) {
            return new DataPackage(Command.DISCONNECT, new Disconnect(userSelfObject));
        } else {
            System.err.println("DataPackageFactory.getDisconnectDataPackage(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getTextMessage(String text, User userSelfObject, int recipientId) {
        if ( text != null & userSelfObject != null ) {
            return new DataPackage(Command.TEXTMESSAGE, new TextMessage(text, userSelfObject.getName(), userSelfObject.id, recipientId));
        } else {
            System.err.println("DataPackageFactory.getDisconnectDataPackage(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getUserGoesOffline(User userObjectWhoGoesOffline) {
        if ( userObjectWhoGoesOffline != null ) {
            return new DataPackage(Command.USERGOESOFFLINE, userObjectWhoGoesOffline);
        } else {
            System.err.println("DataPackageFactory.getUserGoesOffline(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getUserGoesOnline(User userObjectWhoGoesOnline) {
        if ( userObjectWhoGoesOnline != null ) {
            return new DataPackage(Command.USERGOESONLINE, userObjectWhoGoesOnline);
        } else {
            System.err.println("DataPackageFactory.getUserGoesOffline(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getChangeUserName(User userObject, String newNameUserObject) {
        if ( userObject != null &&  newNameUserObject != null ) {
            return new DataPackage(Command.CHANGEUSERNAME, new ChangeUserName(userObject, newNameUserObject));
        } else {
            System.err.println("DataPackageFactory.getChangeUserName(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getChangeUserNameResponse(ChangeUserNameResponseState state) {
            return new DataPackage(Command.CHANGEUSERNAMERESPONSE, state);
    }

    public DataPackage getChangeUserPassword(User userObject, String newPasswordUserObject) {
        if ( userObject != null &&  newPasswordUserObject != null ) {
            return new DataPackage(Command.CHANGEUSERPASSWORD, new ChangeUserPassword(userObject, newPasswordUserObject));
        } else {
            System.err.println("DataPackageFactory.getChangeUserPassword(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getChangeUserPasswordResponse(ChangeUserPasswordResponseState state) {
        return new DataPackage(Command.CHANGEUSERNAMERESPONSE, state);
    }


    public DataPackage getChangeUserPhoto(Object newUserPhoto) {
        if ( newUserPhoto != null ) {
            return new DataPackage(Command.CHANGEUSERPHOTO, new ChangeUserPhoto(newUserPhoto));
        } else {
            System.err.println("DataPackageFactory.getChangeUserPhoto(): Не могу создать DataPackage. Connection == null.");
        }

        return null;
    }

    public DataPackage getChangeUserPhotoResponse(ChangeUserPhotoResponseState state) {
        return new DataPackage(Command.CHANGEUSERPHOTORESPONSE, state);
    }
}
