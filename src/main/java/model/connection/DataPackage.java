package model.connection;

import model.commands.Command;

import java.io.Serializable;


public class DataPackage implements DataPackageDAO, Serializable, Cloneable {
    Command command = null;
    Object objDataPackage = null;
    //ToDo: add dataPackageID (long). And add check messages delivery by dataPackageID

    /*public DataPackage() {
        this.command = Command.HELLORESPONSE;
        objDataPackage = "SomeTextFromDataPackageNotInitializedField_objDataPackage__DEFAULT_CONSTRUCTOR";
    }*/

    public DataPackage(Command command) {
        this.command = command;
        //objDataPackage = "SomeTextFromDataPackageNotInitializedField_objDataPackage";
    }

    public DataPackage(Command command, Object objDataPackage) {
        this.command = command;
        this.objDataPackage = objDataPackage;
    }

    @Override
    public Object getData() {
        if ( objDataPackage != null ) {
            return objDataPackage;
        } else {
            return command.toString();
        }
    }

    @Override
    public Object getData(Object dataPackageObject) {
        return "Test string from DataPackage.runCommand(Object dataPackageObject);";
    }

    public String getCommandName() {
        return command.toString();
    }
}
