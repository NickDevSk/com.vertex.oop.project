package model.connection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;


public class SendConnectorThread extends Thread {
    Socket socket = null;
    private ObjectOutputStream oos = null;
    private volatile boolean connectionAlive = false;
    private volatile boolean threadCanBeStop = false;
    DataPackage dataPackage = null;
    ConcurrentLinkedQueue<? extends DataPackage> sendDataPackageQueue = null;
    Connection connection = null;

    public SendConnectorThread(Socket socket, ConcurrentLinkedQueue<? extends DataPackage> sendDataPackageQueue, Connection connection) {
        this.socket = socket;
        this.sendDataPackageQueue = sendDataPackageQueue;
        connectionAlive = true;
        this.connection = connection;

        String tmpThreadName = this.getName();
        this.setName("SendConnectorThread_" + tmpThreadName);
    }

    @Override
    public void run() {
        if ( socket != null ) {
            try {
                oos = new ObjectOutputStream(socket.getOutputStream());
                oos.flush();

                while ( connectionAlive ) {
                    if ( sendDataPackageQueue.peek() != null ) {
                        dataPackage = sendDataPackageQueue.poll();

                        oos.writeObject(dataPackage);
                        oos.flush();
                    }
                }

                while ( ! threadCanBeStop ) {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                System.err.println("Ошибка во время работы с коннектором отправки.");
                e.printStackTrace();
            } finally {
                if ( oos != null ) {
                    try {
                        oos.close();
                        while ( ! connection.setObjectOutputStreamClosed(true) ) {

                        }
                    } catch (IOException e) {
                        System.err.println("Ошибка во время попытки закрытия коннектора отправки.");
                        e.printStackTrace();
                    }
                }
            }
        } else {
            System.err.println(this.getName() + " : Не могу запустить поток коннектора отправки. Сокет == null.");
        }
    }

    public boolean isConnectionAlive() {
        return connectionAlive;
    }

    public void setConnectionAlive(boolean connectionAlive) {
        this.connectionAlive = connectionAlive;
    }

    public boolean isThreadCanBeStop() {
        return threadCanBeStop;
    }

    public void setThreadCanBeStop(boolean threadCanBeStop) {
        this.threadCanBeStop = threadCanBeStop;
    }
}
