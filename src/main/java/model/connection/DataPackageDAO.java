package model.connection;


public interface DataPackageDAO <T, V> {
    T getData();
    T getData(V dataPackageObject);
}
