package model.client.core;

import model.admin.Admin;
import model.connection.Connection;
import model.connection.DataPackageFactory;
import model.server.core.ServerStart;
import model.user.User;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClientStart {

    static private String srvSocketIP = "192.168.1.189";

    static private int srvSocketPort = 10_001;
    static private boolean connected = false;
    static List<User> userOnlineList = new ArrayList<User>();
    static final public boolean isServer = false;

    {
        //ToDo: add check of quantity Command-s in ENUM, and real quantity in ConnectorWorker (create ArrayList with all command classes, and compare, that all are there.

    }




    public static void main(String[] args) {
        Admin admin = new Admin();
        try {
            //debug
            System.err.println("Client socket - initialized.");

            boolean clientProgrammIsRunning = true;

            Socket socket = new Socket(srvSocketIP, srvSocketPort);

            Connection connection = new Connection(socket);

            ClientWorker clientWorker = new ClientWorker(connection);
            clientWorker.start();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //test work
            DataPackageFactory dataPackageFactory = new DataPackageFactory(connection);

            clientWorker.sendDataPackage(dataPackageFactory.getHelloDataPackage());
            clientWorker.sendDataPackage(dataPackageFactory.getHelloDataPackage());
            clientWorker.sendDataPackage(dataPackageFactory.getHelloDataPackage());
            //test ok

            for ( int i = 7; i > 0; i-- ) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i);
            }



            //test chat
            User tmpUser = new User();
            System.out.print("Введите свой никнэйм: ");
            Scanner scanner = new Scanner(System.in);
            String nickName = scanner.nextLine();
            tmpUser.setName(nickName);
            System.out.println("Можете начинать печатать сообщения в общий чат. :) Для выхода, наберите EXIT и отправьте");
            String text = "start";

//            while ( clientProgrammIsRunning ) {
            while ( ! text.equals("EXIT") ) {
                //test chat
                System.out.print(nickName + ": ");
                text = scanner.nextLine();

                clientWorker.sendDataPackage(dataPackageFactory.getTextMessage(text,tmpUser, 0));
            }

            if ( text.equals("EXIT") ) {
                connection.closeConnection();
                clientWorker.setClientWorkerWorks(false);
            }

            clientWorker.setClientWorkerWorks(false);

        } catch (IOException e) {
            System.err.println("ClientStart socket - error.");
            e.printStackTrace();
        }


        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //debug
        if ( ServerStart.debugMode & ServerStart.checkCounter() ) { System.err.println("ClientStart socket - initialized."); }




    }
}
