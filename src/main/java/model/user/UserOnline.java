package model.user;


public class UserOnline {
    User userOnline = null;
    private int userConnectionId;

    public UserOnline(User userOnline, int userConnectionId) {
        this.userOnline = userOnline;
        this.userConnectionId = userConnectionId;
    }

    public User getUser() {
        return userOnline;
    }

    public void setUser(User userOnline) {
        this.userOnline = userOnline;
    }

    public int getUserConnectionId() {
        return userConnectionId;
    }

    public void setUserConnectionId(int userConnectionId) {
        this.userConnectionId = userConnectionId;
    }
}
