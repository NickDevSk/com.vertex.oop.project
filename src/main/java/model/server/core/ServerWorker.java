package model.server.core;

import model.commands.Command;
import model.commands.dataOfDataPackage.hello.Hello;
import model.commands.dataOfDataPackage.hello.HelloResponse;
import model.commands.dataOfDataPackage.textMessage.TextMessage;
import model.connection.Connection;
import model.connection.DataPackage;
import model.connection.DataPackageFactory;
import model.server.users.UserDB;
import model.user.User;
import model.user.UserOnline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Данный класс, отвечает за:
 *  - работу\управление всеми сетевыми соединениями (содержит список, всех ConnectionThread, с пользователями установившими соединение с сервером).
 *  - Обрабатывать приходящие пакеты-команды (те пакеты, которые приходят на сервер от клиентов).
 *  - Как следствие, может делать пересылку сообщений, всем пользователям (когда один пользователь пишет в общий чат сообщение, его пересылка остальным пользователям ложится на это класс).
 *  - Ведет список пользователей онлайн. (При уходе кого-то в оффлайн, уведомляет остальных об этом).
 *  - Выпонляет авторизацию и регистрацию пользователей (как следствие, управляеи всей базой пользователей).
 */

public class ServerWorker extends Thread {
//    List<Connection> connectionsList = null;    //ArrayList<>();  //doesnt work correct in multy thread env.
    ConcurrentLinkedQueue<Connection> connectionsQueue = null;    //ArrayList<>();
    List<UserOnline> usersOnlineList = null;          //ArrayList<>(); //User + connectionId = UserOnline
    UserDB userDB = null;
    private volatile boolean serverWorkerWorks = false;
    DataPackage dataPackage = null;
    DataPackageFactory dataPackageFactory = null;
    public ConcurrentLinkedQueue<Connection> newConnectionQueue = null;

    //ToDo: Таня, надо реализовать методы в этом классе.
    //ToDo: проинициализировать объекты выше.
    public ServerWorker() {
        serverWorkerWorks = true;
        connectionsQueue = new ConcurrentLinkedQueue<Connection>();
        newConnectionQueue = new ConcurrentLinkedQueue<Connection>();

        String tmpThreadName = this.getName();
        this.setName("ServerWorker_" + tmpThreadName);
    }

    @Override
    public void run() {
        while ( serverWorkerWorks ) {
            if ( newConnectionQueue.peek() != null ) {
                connectionsQueue.add(newConnectionQueue.poll());
            }

            Iterator<Connection> connectionIterator = connectionsQueue.iterator();
            while ( connectionIterator.hasNext() ) {
                Connection tmpCon = connectionIterator.next();
                dataPackageFactory = new DataPackageFactory(tmpCon);

                if ( tmpCon.receivedDataPackageQueue.peek() != null ) {
                    dataPackage = tmpCon.receivedDataPackageQueue.poll();

                    if ( ! executeCommand(dataPackage, tmpCon) ) {
                        System.err.println("Error of execute DataPackage: connectionId: " + tmpCon.connectionId + ", Remote Address: " + tmpCon.getConnectionRemoteSocketAddress());
                    }
                }

                //ToDo: надо реализовать этот методы. Сейчас не выполняется никаких проверок. + Не происходит авторизация. + Не добавляется пользователь в список usersOnlineList
                while ( newConnectionQueue.peek() != null ) {
                    connectionsQueue.add(newConnectionQueue.poll());
                }
            }
        }
    }

    private void closeConnection(int connectionId) {
        Iterator<Connection> connectionIterator = connectionsQueue.iterator();
        while ( connectionIterator.hasNext() ) {
            Connection tmpCon = connectionIterator.next();

            if ( tmpCon.connectionId == connectionId ) {
                tmpCon.closeConnection();
                connectionIterator.remove();
                break;
            }
        }
        //найти среди коннекшенов искомый.
        //вызвать для него коннекшинИД.closeConnection()
        //удалить из connectionsList. (через Итератор.remove(); )
    }

    private boolean executeCommand(DataPackage dataPackage, Connection connection) {
        if ( dataPackage.getCommandName().equals(Command.HELLO.toString()) ) {
            Hello tmpHello = (Hello) dataPackage.getData();
            System.out.println("HELLO, from " + tmpHello.getHostInetAddress());
            connection.sendDataPackage(dataPackageFactory.getHelloResponseDataPackage());

            return true;
        } else if ( dataPackage.getCommandName().equals(Command.HELLORESPONSE.toString()) ) {
            HelloResponse tmpHelloResponse = (HelloResponse) dataPackage.getData();
            System.out.println("HELLORESPONSE, from :" + tmpHelloResponse.getHostInetAddress());

            return true;
        } else if ( dataPackage.getCommandName().equals(Command.TEXTMESSAGE.toString()) ) {
            // теперь, можем выполнить проверки, полученного текста. Допустимый ли получатель. Есть ли такой отправильтель. Нету ли запрещенных слов в текстовом поле.
            // Если все ок - отправляем дальше. Если нет - генерируем ответ ошибку.

            //debug_Server_sout_of_received_messages
            TextMessage tmp = (TextMessage) dataPackage.getData();
            System.err.println(tmp.getTextMessage());

            //Сейчас тестим. Шлем месседж всем. Без проверок.
            forwardTextMessageToAll(dataPackage, connection);
//            System.out.println("SENDTEXTMESSAGE, came from :");
            //connection.sendDataPackage(new DataPackage(Command.SENDTEXTMESSAGERESPONSE, "OK"));

            //ToDo:
            //Не плохо было бы, в случае смены пользователем имени, обновить эту инфу и на сервере, и в чате переписки.
            return true;
        } else if ( dataPackage.getCommandName().equals(Command.DISCONNECT.toString()) ) {
            //Изменения буду делать НЕ через итератор. (Хотя данные нашел, и передал сюда по итератору.
            // Вопрос: При вызове closeConnection, не приведет ли это к изменению объекта коллекции, и как следствие, к ошибке и падению?

            //debug!!!
            System.err.println("___RECIEVED DISCONNECT PACKAGE!!!___");

            connection.sendDataPackage(new DataPackage(Command.DISCONNECT));
            closeConnection(connection.connectionId);

            //ToDo:
            //При разрыве соединения, надо сохранить всю инфу о пользователе. Удалить из списка онлайн пользователей.
            return true;
        }

        return false;
    }

    private void forwardTextMessageToAll(DataPackage sendTextMessage, Connection connection) {
        int messageComeFromConnectionId = connection.connectionId;

        Iterator<Connection> connectionIterator = connectionsQueue.iterator();
        while ( connectionIterator.hasNext() ) {
            Connection tmpCon = connectionIterator.next();

            if ( tmpCon.connectionId != messageComeFromConnectionId ) {
                tmpCon.sendDataPackage(sendTextMessage);
            }
        }
    }

    private User getUserOnlineByConnectionId(int searchUserConnectionId) {
        for (UserOnline tmp : usersOnlineList) {
            if ( tmp.getUserConnectionId() == searchUserConnectionId ) {
                return tmp.getUser();
            }
        }

        return null;
    }
}
