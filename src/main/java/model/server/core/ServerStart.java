package model.server.core;

import model.connection.Connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerStart {
    final static public Integer serverId = new Integer(0);
    static private int srvSocketPort = 10_001;
    static private boolean srvIsWork = true;
    static final public boolean isServer = true;

    //debug config
    static public int counter = 0;
    static public int counterMaxMessages = 300;
    static public boolean debugMode = false;

    //debug string
//    if (ServerStart.checkCounter() & ServerStart.debugMode ) { System.err.println("start CLASS-NAME_OR_METHOD-NAME_IN_CLASSNAME"); }

    public static void main(String[] args) {
        try {
            Thread.sleep(2_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            //debug
            System.err.println("Server socket - initialized.");

            ServerSocket srvSocket = new ServerSocket(srvSocketPort);

            ServerWorker serverWorker = new ServerWorker();
            serverWorker.start();

            while ( srvIsWork ) {
                Socket socket = srvSocket.accept();
                Connection tmpCon = new Connection(socket);

                //serverWorker.newConnectionQueue.offer(tmpCon);
                while ( ! serverWorker.newConnectionQueue.offer(tmpCon)) {

                }
            }

            srvSocket.close();
        } catch (IOException e) {
            System.err.println("Server socket - error.");
            e.printStackTrace();
        }



        //ToDo: write part, of saving and restore from\to HDD all data (messages logs), and userDB. Server logs (if is).
    }

    public int getSrvSocketPort() {
        return srvSocketPort;
    }

    public void setSrvSocketPort(int srvSocketPort) {
        this.srvSocketPort = srvSocketPort;
    }

    public static boolean isSrvIsWork() {
        return srvIsWork;
    }

    public static void setSrvIsWork(boolean srvIsWork) {
        ServerStart.srvIsWork = srvIsWork;
    }

    synchronized public static boolean checkCounter() {
        if ( counter < counterMaxMessages ) {
            counter += 1;

            return true;
        } else {
            return false;
        }
    }
}
