package model.commands.dataOfDataPackage.hello;


import java.io.Serializable;

public class HelloResponse implements Serializable {
    String hostInetAddress = null;

    public HelloResponse(String hostInetAddress) {
        if ( hostInetAddress != null ) {
            this.hostInetAddress = hostInetAddress;
        } else {
            this.hostInetAddress = "Host IP address is null";
        }
    }

    public String getHostInetAddress() {
        return hostInetAddress;
    }
}
