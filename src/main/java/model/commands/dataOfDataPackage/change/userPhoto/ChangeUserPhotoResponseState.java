package model.commands.dataOfDataPackage.change.userPhoto;


import java.io.Serializable;

public enum ChangeUserPhotoResponseState implements Serializable {
    OK,
    ILLEGAL_NEW_PHOTO_FORMAT,   //psd, pdf, exim, etc.
    NEW_PHOTO_SIZE_TOO_LARGE
}
