package model.commands.dataOfDataPackage.change.userPhoto;


import java.io.Serializable;

public class ChangeUserPhoto implements Serializable {
    Object newUserPhoto = null;

    public ChangeUserPhoto(Object newUserPhoto) {
        this.newUserPhoto = newUserPhoto;
    }

    public Object getNewUserPhoto() {
        return newUserPhoto;
    }
}
