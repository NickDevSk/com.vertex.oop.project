package model.commands.dataOfDataPackage.change.userPhoto;


import java.io.Serializable;

public class ChangeUserPhotoResponse implements Serializable {
    ChangeUserPhotoResponseState userPhotoResponseState = null;

    public ChangeUserPhotoResponse(ChangeUserPhotoResponseState userPhotoResponseState) {
        this.userPhotoResponseState = userPhotoResponseState;
    }

    public ChangeUserPhotoResponseState getUserPhotoResponseState() {
        return userPhotoResponseState;
    }
}
