package model.commands.dataOfDataPackage.change.userPassword;


import java.io.Serializable;

public class ChangeUserPasswordResponse implements Serializable {
    ChangeUserPasswordResponseState state = null;

    public ChangeUserPasswordResponse(ChangeUserPasswordResponseState changeUserPasswordResponseState) {
        this.state = changeUserPasswordResponseState;
    }

    public ChangeUserPasswordResponseState getState() {
        return state;
    }
}
