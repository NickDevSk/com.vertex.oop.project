package model.commands.dataOfDataPackage.change.userPassword;


import java.io.Serializable;

public enum ChangeUserPasswordResponseState implements Serializable {
    OK,
    ILLEGAL_SYMBOLS_IN_NEW_PASSWORD,
    TOO_LONG_NEW_PASSWORD,
    NEW_PASSWORD_IS_EMPTY,
    NOT_STRONG_ENOUGH_PASSWORD
}
