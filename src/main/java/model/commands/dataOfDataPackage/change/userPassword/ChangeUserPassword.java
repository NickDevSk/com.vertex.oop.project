package model.commands.dataOfDataPackage.change.userPassword;

import model.user.User;

import java.io.Serializable;


public class ChangeUserPassword implements Serializable {
    User user = null;
    String newUserPassword = null;

    public ChangeUserPassword(User user, String newUserPassword) {
        this.user = user;
        this.newUserPassword = newUserPassword;
    }

    public User getUser() {
        return user;
    }

    public String getNewUserPassword() {
        return newUserPassword;
    }
}
