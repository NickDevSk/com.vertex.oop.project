package model.commands.dataOfDataPackage.change.userName;


import java.io.Serializable;

public enum ChangeUserNameResponseState implements Serializable {
    OK,
    ILLEGAL_SYMBOLS_IN_NEW_NAME,
    TOO_LONG_NEW_NAME,
    NEW_NAME_IS_EMPTY,
    NEW_NAME_IS_ALLREADY_IN_USE
}
