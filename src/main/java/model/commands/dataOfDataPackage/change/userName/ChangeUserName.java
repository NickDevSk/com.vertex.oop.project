package model.commands.dataOfDataPackage.change.userName;

import model.user.User;

import java.io.Serializable;

public class ChangeUserName implements Serializable {
    User user = null;
    String newUserName = null;

    public ChangeUserName(User user, String newUserName) {
        this.user = user;
        this.newUserName = newUserName;
    }

    public User getUser() {
        return user;
    }

    public String getNewUserName() {
        return newUserName;
    }
}
