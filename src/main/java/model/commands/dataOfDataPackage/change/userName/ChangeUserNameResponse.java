package model.commands.dataOfDataPackage.change.userName;


import java.io.Serializable;

public class ChangeUserNameResponse implements Serializable {
    ChangeUserNameResponseState state = null;

    public ChangeUserNameResponse(ChangeUserNameResponseState changeUserNameResponseState) {
        this.state = changeUserNameResponseState;
    }

    public ChangeUserNameResponseState getState() {
        return state;
    }
}
