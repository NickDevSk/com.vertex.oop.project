package model.commands.dataOfDataPackage.authorize;

import model.user.User;

import java.io.Serializable;

public class Authorize implements Serializable {
    User currentUser = null;

    public Authorize(User user) {
        currentUser = user;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
