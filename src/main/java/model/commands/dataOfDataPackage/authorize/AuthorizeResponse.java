package model.commands.dataOfDataPackage.authorize;

import java.io.Serializable;

public class AuthorizeResponse implements Serializable {
    AuthorizeResponseState response = null;

    public AuthorizeResponse(AuthorizeResponseState response) {
        this.response = response;
    }

    public AuthorizeResponseState getResponse() {
        return response;
    }
}
