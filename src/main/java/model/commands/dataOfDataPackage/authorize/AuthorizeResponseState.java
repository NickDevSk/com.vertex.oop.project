package model.commands.dataOfDataPackage.authorize;


import java.io.Serializable;

public enum AuthorizeResponseState implements Serializable {
    OK,
    NO_SUCH_USER,
    INCORRECT_LOGIN_OR_PASSWORD,
    PASSWORD_EXPIRED // :) for future
}
