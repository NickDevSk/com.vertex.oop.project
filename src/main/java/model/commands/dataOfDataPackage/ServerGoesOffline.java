package model.commands.dataOfDataPackage;


import java.io.Serializable;

public class ServerGoesOffline implements Serializable {
    String state = ": goes offline.";
    String serverId = null;

    public ServerGoesOffline(String serverId) {
        this.serverId = serverId;
    }

    public String getState() {
        return "Server: " + serverId + state;
    }
}
