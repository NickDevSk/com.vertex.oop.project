package model.commands.dataOfDataPackage.userGoes;


import model.user.User;

import java.io.Serializable;

public class UserGoesOnline implements Serializable {
    User userGoesOnline = null;

    public UserGoesOnline(User userGoesOnline) {
        this.userGoesOnline = userGoesOnline;
    }

    public User getUserGoesOnline() {
        return userGoesOnline;
    }
}
