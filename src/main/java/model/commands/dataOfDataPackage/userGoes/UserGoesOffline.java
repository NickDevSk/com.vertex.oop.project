package model.commands.dataOfDataPackage.userGoes;


import java.io.Serializable;

public class UserGoesOffline implements Serializable {
    int userIdGoesOffline;

    public UserGoesOffline(int userIdGoesOffline) {
        this.userIdGoesOffline = userIdGoesOffline;
    }

    public int getUserIdGoesOffline() {
        return userIdGoesOffline;
    }
}
