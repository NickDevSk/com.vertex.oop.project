package model.commands.dataOfDataPackage;


import model.user.User;

import java.io.Serializable;

public class Disconnect implements Serializable {
    User user = null;
    String state = ": goes offline.";

    public Disconnect(User user) {
        this.user = user;
    }

    public String getState() {
        return (user.getName() + state);
    }
}
