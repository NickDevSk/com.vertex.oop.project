package model.commands.dataOfDataPackage.register;


import model.user.User;

import java.io.Serializable;


public class Register implements Serializable {
    User newUser = null;

    public Register(User newUser) {
        this.newUser = newUser;
    }

    public User getNewUser() {
        return newUser;
    }
}
