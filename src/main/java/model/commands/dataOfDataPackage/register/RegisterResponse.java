package model.commands.dataOfDataPackage.register;


import java.io.Serializable;

public class RegisterResponse implements Serializable {
    RegisterResponseState registerResponseState = null;
    private int userIdRecievedFromServer;

    public RegisterResponse(RegisterResponseState registerResponseState, int userIdRecievedFromServer) {
        this.registerResponseState = registerResponseState;
        this.userIdRecievedFromServer = userIdRecievedFromServer;
    }

    public RegisterResponseState getRegisterResponseState() {
        return registerResponseState;
    }

    public int getUserIdRecievedFromServer() {
        return userIdRecievedFromServer;
    }
}
