package model.commands.dataOfDataPackage.register;


import java.io.Serializable;

public enum RegisterResponseState implements Serializable {
    OK,
    ILLEGAL_SYMBOLS_IN_NAME,
    TOO_LONG_NAME,
    NAME_IS_EMPTY,
    NAME_IS_ALLREADY_IN_USE,
    ILLEGAL_SYMBOLS_IN_PASSWORD,
    TOO_LONG_PASSWORD,
    PASSWORD_IS_EMPTY,
    NOT_STRONG_ENOUGH_PASSWORD,
    ILLEGAL_PHOTO_FORMAT,   //psd, pdf, exim, etc.
    PHOTO_SIZE_TOO_LARGE
}
