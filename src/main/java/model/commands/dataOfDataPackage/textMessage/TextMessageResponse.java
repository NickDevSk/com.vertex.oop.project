package model.commands.dataOfDataPackage.textMessage;


import java.io.Serializable;

public class TextMessageResponse implements Serializable {
    TextMessageResponseState textMessageResponseState = null;

    public TextMessageResponse(TextMessageResponseState textMessageResponseState) {
        this.textMessageResponseState = textMessageResponseState;
    }

    public TextMessageResponseState getTextMessageResponseState() {
        return textMessageResponseState;
    }
}
