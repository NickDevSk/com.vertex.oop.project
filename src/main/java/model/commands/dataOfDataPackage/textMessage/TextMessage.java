package model.commands.dataOfDataPackage.textMessage;


import java.io.Serializable;

public class TextMessage implements Serializable {
    String textMessage = null;
    String senderName = null;
    int senderId;
    int recipientId;

    public TextMessage(String textMessage, String senderName, int senderId, int recipientId) {
        this.textMessage = textMessage;
        this.senderName = senderName;
        this.senderId = senderId;
        this.recipientId = recipientId;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public int getSenderId() {
        return senderId;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public String getSenderName() {
        return senderName;
    }
}
