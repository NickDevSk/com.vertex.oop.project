package model.commands.dataOfDataPackage.textMessage;


import java.io.Serializable;

public enum TextMessageResponseState implements Serializable {
    OK,
    CANT_FIND_SUCH_RECIPIENT_ID,
    ILLEGAL_TEST_FIELD_CONTENT,
    ILLEGAL_SENDER_ID
}
