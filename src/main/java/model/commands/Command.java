package model.commands;

public enum Command {
    AUTHORIZE,
    AUTHORIZERESPONSE,
    CHANGEUSERNAME,
    CHANGEUSERNAMERESPONSE,
    CHANGEUSERPASSWORD,
    CHANGEUSERPASSWORDRESPONSE,
    CHANGEUSERPHOTO,
    CHANGEUSERPHOTORESPONSE,
    COMMANDTRANSFERSTATE,
    DISCONNECT,
    HELLO,
    HELLORESPONSE,
    REGISTER,
    REGISTERRESPONSE,
    TEXTMESSAGE,
    TEXTMESSAGERESPONSE,
    SERVERGOESOFFLINE,
    USERGOESOFFLINE,
    USERGOESONLINE
}
